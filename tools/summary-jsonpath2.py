import json
import os
import csv
import pandas as pd
from jsonpath_ng import jsonpath, parse
import sys
import numpy as np

if __name__ == "__main__":
    input_dir = sys.argv[1]

    csvdata = {'scenario': [], 'instance': [], 'ev' : [], 'alg': [], 'seed' : [], 'tl' : [], 'rt' : [], 'ttt' : [], 'eff' : [], 'imp' : [], 'perc' : [], 'teleported' : []}

    scenarios = ['sp','ny']
    instances = [1,2,3,4,5]

    exclude_alg = ['smartcity-centered']
    include_ev = {'sp' : 'veh11651', 'ny' : 'veh4856'}

    for scenario in scenarios:
        for instance in instances:
            base_path = '{0}/{1}/{1}-{2}/results/staticdynamic'.format(input_dir,scenario,instance)

            for r in os.listdir(base_path):
                if r.split('.')[-1] != 'json':
                    continue

                params = ''.join(r.split('.')[0:-1]).split('!')
                alg = params[1].split('_')[0]

                if alg in exclude_alg:
                    continue

                ev = params[2].split('_')[0]

                if ev not in include_ev[scenario]:
                    continue

                seed = params[3].split('_')[0]
                args = ''

                if '_' in params[3]:
                    args = '{}!{}'.format(params[3].split('_')[1],'!'.join(params[4:]))  

                print('processing {}/{}...'.format(base_path,r))

                tmp = open('{}/{}'.format(base_path,r),'r')
                data = json.loads(tmp.read())
                tmp.close()               

                teleported = data['summary']['ev_was_teleported']
                
                csvdata['scenario'].append(scenario)
                csvdata['instance'].append(instance)
                csvdata['ev'].append(ev)
                csvdata['alg'].append('{}{}'.format(alg,'' if args == '' else '_{}'.format(args))   )
                csvdata['seed'].append(seed)
                csvdata['teleported'].append(teleported)
                csvdata['rt'].append(np.NaN)
                csvdata['imp'].append(np.NaN)
                csvdata['perc'].append(np.NaN)
                if not teleported:
                    csvdata['tl'].append(data['summary']['timeloss-ev'])
                    csvdata['ttt'].append(data['summary']['ttt-ev'])
                    csvdata['eff'].append(float(data['summary']['ttt-ev']) - float(data['summary']['timeloss-ev']))
                else:
                    csvdata['tl'].append(np.NaN)
                    csvdata['ttt'].append(np.NaN)
                    csvdata['eff'].append(np.NaN)            


                data = None

    df = pd.DataFrame(csvdata)

    df_other = df[(df['alg'] != 'no-preemption') & (df['teleported'] != True)]

    tl_imp = []

    for index, row in df_other.iterrows():
        df_nopreempt = df[(df['alg'] == 'no-preemption') & (df['scenario'] == row.scenario) & (df['ev'] == row.ev) & (df['seed'] == row.seed) & (df['teleported'] != True)]

        if not df_nopreempt.empty:
            tl_before = float(df_nopreempt['tl'].iloc[0])
            tl_after = float(row.tl)
            df.loc[index,'imp'] = tl_before/tl_after if tl_after < tl_before else  -1*(tl_after/tl_before)
            df.loc[index,'perc'] = (1-(tl_after/tl_before))*100

    df.to_csv(sys.argv[2],index=False)