from classes.preemption_strategy import PreemptionStrategy

from sumolib import checkBinary  # noqa
import traci  # noqa

class AllGreenStrategy(PreemptionStrategy):
    #constants: Marshall, P.S. and W.D. Berg, Design Guidelines for Railroad Preemption at Signalized Intersections. Institute of Transportation Engineers Journal, 1997.
    def configure(self):
        self.infinity = True
        self.started = False
        self.restored = False
        self.ev_entered = False
        self.ev_exited = False         

    def execute_step(self,step,ev_entered_in_simulation):
        super().execute_step(step,ev_entered_in_simulation)
        #self.sync_tls(step)

        if not self.ev_entered and ev_entered_in_simulation:
            self.ev_entered = True

        if self.ev_entered and self.ev not in traci.vehicle.getIDList():
            self.ev_exited = True        

        if self.ev_entered and not self.ev_exited:
            if not self.started:
                #for tl in self.conf.tls:
                for edge in self.conf.edges_with_tl:
                    self.open_tl_at_time_by_cycles(1,edge,step)

                self.started = True

        if self.ev_entered and self.ev_exited and not self.restored:
            #for tl in self.conf.tls:
            for edge in self.conf.edges_with_tl:
                #self.orch.schedule_sync(self.ev,tl,self.conf,self.infinity)
                self.tls_to_sync[edge] = 1

            self.restored = True